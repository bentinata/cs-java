import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;

public class Target extends Polygon {
  private DoubleProperty x;
  private DoubleProperty y;
  private DoubleProperty v;

  public Target(double x, double y, double v) {
    super();

    this.x = new SimpleDoubleProperty(x);
    this.y = new SimpleDoubleProperty(y);
    this.v = new SimpleDoubleProperty(v);

    double outer = v;
    if (outer < 10) outer = 10;
    if (outer > 25) outer = 25;
    double inner = (outer * 1 / 2) + (Math.random() * outer * 1 / 5);
    for (int i = 0; i < 5; i++) {
      getPoints().add(outer * Math.cos(i * 2 * Math.PI / 5));
      getPoints().add(outer * Math.sin(i * 2 * Math.PI / 5));

      getPoints().add(inner * Math.cos((i * 2 + 1) * Math.PI / 5));
      getPoints().add(inner * Math.sin((i * 2 + 1) * Math.PI / 5));
    }
    setLayoutX(x);
    setLayoutY(y);

    Color color = Color.WHITE;
    if (v <= 5)
      color = Color.color(
        Math.random() * .2,
        Math.random() * .2,
        Math.random() * .2
      );
    else if (v > 5 && v <= 10)
      color = Color.color(
        Math.random() * .2,
        .5 + Math.random() * .5,
        Math.random() * .2
      ).saturate();
    else if (v > 10)
      color = Color.color(
        .5 + Math.random() * .5,
        .5 + Math.random() * .5,
        Math.random() * .2
      ).saturate().saturate().saturate();

    setFill(color);
    setStroke(color.darker());
    setStrokeWidth(2);
    setRotate(Math.random() * 360);
  }

  public double getX() {
    return x.get();
  }

  public void setX(double x) {
    this.x.set(x);
  }

  public DoubleProperty xProperty() {
    return x;
  }

  public double getY() {
    return y.get();
  }

  public void setY(double y) {
    this.y.set(y);
  }

  public DoubleProperty yProperty() {
    return y;
  }

  public double getV() {
    return v.get();
  }

  public void setV(double v) {
    this.v.set(v);
  }

  public DoubleProperty vProperty() {
    return v;
  }
}
