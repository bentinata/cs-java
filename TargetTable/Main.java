import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Path;
import javafx.scene.shape.Shape;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.ArrayList;

public class Main extends Application {
  @Override
  public void start(Stage stage) {
    stage.setTitle("No title specification.");

    Insets inset = new Insets(8, 8, 8, 8);
    BorderPane frontPane = new BorderPane();
    BorderPane tablePane = new BorderPane();
    BorderPane mainPane = new BorderPane();

    Pane pool = new Pane();
    pool.setStyle("-fx-background-color: white");
    mainPane.setCenter(pool);

    Scene scene = new Scene(frontPane, 600, 400);

    // Configure frontPane.
    frontPane.setPadding(inset);
    Label title = new Label("<Nama Games>");
    BorderPane.setAlignment(title, Pos.CENTER);
    frontPane.setTop(title);

    VBox vbox = new VBox();
    vbox.setPadding(inset);
    vbox.setSpacing(inset.getTop());
    vbox.setAlignment(Pos.CENTER);
    frontPane.setCenter(vbox);

    Button start = new Button("Start");
    start.setOnAction(e -> scene.setRoot(tablePane));
    vbox.getChildren().add(start);


    Button exit = new Button("Exit");
    exit.setOnAction(e -> Platform.exit());
    vbox.getChildren().add(exit);

    Label num = new Label("1503846");
    BorderPane.setAlignment(num, Pos.BOTTOM_CENTER);
    frontPane.setLeft(num);

    Label name = new Label("Benget Nata");
    BorderPane.setAlignment(name, Pos.BOTTOM_CENTER);
    frontPane.setRight(name);

    // Configure tablePane.
    VBox tableVbox = new VBox(inset.getTop());
    tableVbox.setPadding(new Insets(inset.getTop(), 60, inset.getBottom(), 60));
    Label tableLabel = new Label("Target coordinate table");
    BorderPane.setAlignment(tableLabel, Pos.CENTER);
    tablePane.setTop(tableLabel);


    TableView<Target> table = new TableView<>();
    ObservableList<Target> targets = FXCollections.observableArrayList();

//    Quick way to generate stars on upper half of scene.
//    for (int i = 0; i < 15; i++) {
//      targets.add(new Target(
//        50 + Math.random() * (scene.getWidth() - 100),
//        50 + Math.random() * scene.getHeight() / 3,
//        Math.random() * 15
//      ));
//    }

    TableColumn<Target, Integer> nCol = new TableColumn<>("number");
    nCol.setCellValueFactory(x -> new ReadOnlyObjectWrapper(table.getItems().indexOf(x.getValue()) + 1));
    TableColumn<Target, Double> xCol = new TableColumn<>("x");
    xCol.setCellValueFactory(x -> x.getValue().xProperty().asObject());
    TableColumn<Target, Double> yCol = new TableColumn<>("y");
    yCol.setCellValueFactory(x -> x.getValue().yProperty().asObject());
    TableColumn<Target, Double> vCol = new TableColumn<>("value");
    vCol.setCellValueFactory(x -> x.getValue().vProperty().asObject());

    table.setItems(targets);
    table.getColumns().setAll(nCol, xCol, yCol, vCol);
    table.setPrefWidth(100);
    tableVbox.getChildren().add(table);

    HBox addHbox = new HBox(inset.getTop());
    addHbox.setAlignment(Pos.CENTER_RIGHT);
    TextField inputX = new TextField();
    inputX.setPromptText("X coordinate");
    inputX.setPrefWidth(120);
    TextField inputY = new TextField();
    inputY.setPromptText("Y coordinate");
    inputY.setPrefWidth(120);
    TextField inputV = new TextField();
    inputV.setPromptText("Target value");
    inputV.setPrefWidth(120);
    Button add = new Button("Add");
    add.setOnAction(e -> {
      if (targets.size() >= 15) return;

      targets.add(new Target(
        Double.parseDouble(inputX.getText()),
        Double.parseDouble(inputY.getText()),
        Double.parseDouble(inputV.getText())
      ));

      inputX.clear();
      inputY.clear();
      inputV.clear();
    });
    add.setPrefWidth(80);
    addHbox.getChildren().addAll(inputX, inputY, inputV, add);
    tableVbox.getChildren().add(addHbox);

    HBox removeHbox = new HBox(inset.getTop());
    removeHbox.setAlignment(Pos.CENTER_RIGHT);
    TextField inputN = new TextField();
    inputN.setPromptText("Number to remove");
    inputN.setPrefWidth(120);
    Button remove = new Button("Remove");
    remove.setOnAction(e -> {
      targets.remove(Integer.parseInt(inputN.getText()) - 1);
      inputN.clear();
    });
    remove.setPrefWidth(80);
    removeHbox.getChildren().addAll(inputN, remove);
    tableVbox.getChildren().add(removeHbox);

    tablePane.setCenter(tableVbox);

    Button startGame = new Button("Start Game");
    BorderPane.setAlignment(startGame, Pos.CENTER);
    tablePane.setBottom(startGame);
    tablePane.getChildren().forEach(node -> BorderPane.setMargin(node, inset)); // Easy margin trick.

    // Configure mainPane.
    Ship ship = new Ship();
    ship.setLayoutX(scene.getWidth() / 2);
    ship.getParts().forEach(part -> part.setLayoutX(scene.getWidth() / 2));
    ship.setLayoutY(scene.getHeight() - 50);
    ship.getParts().forEach(part -> part.setLayoutY(scene.getHeight() - 50));
    pool.getChildren().add(ship);
    pool.getChildren().addAll(ship.getParts());

    // Keypress
    scene.setOnKeyPressed(e -> {
      switch (e.getCode()) {
        case SPACE:
          TranslateTransition bullet = ship.shoot();
          pool.getChildren().add(bullet.getNode());

          bullet.getNode().boundsInParentProperty().addListener((observable, oldValue, newValue) -> {
            for (Target target : new ArrayList<>(targets)) {
              if (((Path) Shape.intersect((Shape) bullet.getNode(), target)).getElements().size() > 0) {
                target.setFill(((Color) target.getFill()).darker());
                target.setStroke(((Color) target.getStroke()).darker());
                target.setV(target.getV() - 1);
                if (target.getV() <= 0) {
                  targets.remove(target);
                  pool.getChildren().remove(target);
                }

                bullet.stop();
                pool.getChildren().remove(bullet.getNode());
              }
            }
          });

          bullet.play();
          break;
        case RIGHT:
          ship.setDirection(ship.DIRECTION_RIGHT);
          break;
        case LEFT:
          ship.setDirection(Ship.DIRECTION_LEFT);
          break;
      }
    });

    scene.setOnKeyReleased(e -> {
      switch (e.getCode()) {
        case RIGHT:
          if (ship.getDirection() == ship.DIRECTION_LEFT) break;
          ship.setDirection(ship.DIRECTION_STAY);
          break;
        case LEFT:
          if (ship.getDirection() == ship.DIRECTION_RIGHT) break;
          ship.setDirection(ship.DIRECTION_STAY);
          break;
      }
    });

    // Show
    stage.setScene(scene);
    stage.show();

    // Animation
    Timeline timeline = new Timeline();
    timeline.setCycleCount(Timeline.INDEFINITE);
    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(1000 / 60), e -> {
      ship.move();

      if (targets.size() == 0)
        Platform.exit();
    }));
    
    startGame.setOnAction(e -> {
      scene.setRoot(mainPane);
      targets.forEach(target -> pool.getChildren().add(target));
      timeline.play();
    });
  }
}
