import javafx.animation.Interpolator;
import javafx.animation.TranslateTransition;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Shape;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.List;

public class Ship extends Polygon {
  public static final int DIRECTION_LEFT = -1;
  public static final int DIRECTION_STAY = 0;
  public static final int DIRECTION_RIGHT = 1;

  private int direction;
  private double speed = 5;
  private List<Shape> parts = new ArrayList<>();

  public Ship() {
    super(
      0, 0,
      5, 5,
      10, 20,
      10, 10,
      20, 30,
      10, 25,
      0, 30,
      -10, 25,
      -20, 30,
      -10, 10,
      -10, 20,
      -5, 5
    );

    Polygon glass = new Polygon(
      0, 0,
      0, 10,
      5, 15,
      5, 20,
      0, 25,
      -5, 20,
      -5, 15,
      0, 10
    );
    glass.setFill(Color.SKYBLUE);
    parts.add(glass);

    setFill(Color.DARKGRAY);
    setStroke(Color.DARKGRAY.darker());
  }

  public double getDirection() {
    return direction;
  }

  public Ship setDirection(int direction) {
    this.direction = direction;
    return this;
  }

  public double getSpeed() {
    return speed;
  }

  public Ship setSpeed(double speed) {
    this.speed = speed;
    return this;
  }

  public List<Shape> getParts() {
    return parts;
  }

  public Ship move() {
    if (getLayoutX() < 0) {
      setLayoutX(0);
      parts.forEach(part -> setLayoutX(0));
    } else if (getLayoutX() > getParent().getLayoutBounds().getWidth()) {
      setLayoutX(getParent().getLayoutBounds().getWidth());
      parts.forEach(part -> setLayoutX(getParent().getLayoutBounds().getWidth()));
    } else {
      setLayoutX(getLayoutX() + direction * speed);
      parts.forEach(part -> part.setLayoutX(getLayoutX()));
    }
    return this;
  }

  public TranslateTransition shoot() {
    Shape bullet = new Circle(2, Color.color(
      .5 + Math.random() * .5,
      .5 + Math.random() * .5,
      .5 + Math.random() * .5
    ).saturate().saturate().saturate());
    TranslateTransition animation = new TranslateTransition(Duration.millis(getLayoutY()), bullet);
    animation.setFromX(getLayoutX());
    animation.setFromY(getLayoutY());
    animation.setToY(0);
    animation.setInterpolator(Interpolator.LINEAR);
    animation.setOnFinished(e -> ((Pane) bullet.getParent()).getChildren().remove(bullet));

    return animation;
  }
}
