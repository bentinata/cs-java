package view;

import control.BookController;
import javafx.collections.ListChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import model.Book;

import java.sql.SQLException;
import java.util.List;

public class View extends Scene {
  public View() throws ClassNotFoundException, SQLException {
    super(new BorderPane(), 600, 400);
    Insets inset = new Insets(8, 8, 8, 8);
    BorderPane root = (BorderPane) getRoot();

//    Top part.
    Label heading = new Label("Babe's Books");
    heading.setPadding(inset);
    BorderPane.setAlignment(heading, Pos.CENTER);
    root.setTop(heading);

//    Center part.
    BookController bookController = new BookController();
    TableView<Book> table = new TableView<>();
    table.setEditable(true);
    table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    table.setItems(bookController.getAll());

    table.getItems().addListener((ListChangeListener<? super Book>) c -> {
      while (c.next()) {
        if (c.wasAdded()) {
          try {
            bookController.addAll((List<Book>) c.getAddedSubList());
          } catch (SQLException e) {
            e.printStackTrace();
          }
        } else if (c.wasRemoved()) {
          try {
            bookController.removeAll((List<Book>) c.getRemoved());
          } catch (SQLException e) {
            e.printStackTrace();
          }
        }
      }
    });

    TableColumn<Book, String> titleCol = new TableColumn<>("Title");
    titleCol.setCellValueFactory(x -> x.getValue().titleProperty());
    titleCol.setCellFactory(TextFieldTableCell.forTableColumn());
    titleCol.setOnEditCommit(e -> {
      e.getTableView().getItems().get(e.getTablePosition().getRow()).setTitle(e.getNewValue());
      try {
        bookController.update(e.getRowValue());
      } catch (SQLException e1) {
        e1.printStackTrace();
      }
    });
    TableColumn<Book, String> authorCol = new TableColumn<>("Author");
    authorCol.setCellValueFactory(x -> x.getValue().authorProperty());
    authorCol.setCellFactory(TextFieldTableCell.forTableColumn());
    authorCol.setOnEditCommit(e -> {
      e.getTableView().getItems().get(e.getTablePosition().getRow()).setAuthor(e.getNewValue());
      try {
        bookController.update(e.getRowValue());
      } catch (SQLException e1) {
        e1.printStackTrace();
      }
    });
    table.getColumns().setAll(titleCol, authorCol);
    root.setCenter(table);


//    Bottom part.
    Label text = new Label(
      "To add data, fill the fields below, then click 'Add' button.\n" +
        "To remove data, select data from table, then click 'Remove'."
    );
    text.setTextAlignment(TextAlignment.CENTER);
    HBox detailHbox = new HBox(inset.getTop(), text);
    detailHbox.setAlignment(Pos.CENTER);

    TextField title = new TextField();
    title.setPromptText("BookController title");
    title.setPrefWidth(120);
    TextField author = new TextField();
    author.setPromptText("Author name");
    author.setPrefWidth(120);
    Button add = new Button("Add");
    add.setPrefWidth(80);
    add.setOnAction(e -> {
      Book book = new Book();
      book.setTitle(title.getText());
      book.setAuthor(author.getText());
      table.getItems().add(book);
      title.clear();
      author.clear();
    });
    HBox addHbox = new HBox(inset.getTop(), title, author, add);
    addHbox.setAlignment(Pos.CENTER_RIGHT);

    Button remove = new Button("Remove");
    remove.setPrefWidth(80);
    remove.setOnAction(e -> {
      table.getItems().removeAll(table.getSelectionModel().getSelectedItems());
    });
    HBox removeHbox = new HBox(inset.getTop(), remove);
    removeHbox.setAlignment(Pos.CENTER_RIGHT);

    VBox vbox = new VBox(inset.getTop(), detailHbox, addHbox, removeHbox);
    vbox.setPadding(inset);
    root.setBottom(vbox);
    root.getChildren().forEach(node -> BorderPane.setMargin(node, inset));
  }
}
