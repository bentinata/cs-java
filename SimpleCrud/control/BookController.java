package control;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Book;

import java.sql.*;
import java.util.List;

public class BookController {
  public BookController() throws ClassNotFoundException {
    Class.forName("org.sqlite.JDBC");
  }

  public ObservableList<Book> getAll() throws SQLException {
    Connection connection = DriverManager.getConnection("jdbc:sqlite:db");
    ObservableList<Book> books = FXCollections.observableArrayList();
    ResultSet resultSet = connection
      .createStatement()
      .executeQuery("select * from daftar_buku");
    while (resultSet.next()) {
      Book book = new Book();
      book.setTitle(resultSet.getString("judul_buku"));
      book.setAuthor(resultSet.getString("penulis_buku"));
      book.setCode(resultSet.getString("kode_buku"));
      books.add(book);
    }

    connection.close();
    return books;
  }

  public void addAll(List<Book> books) throws SQLException {
    Connection connection = DriverManager.getConnection("jdbc:sqlite:db");
    StringBuilder sql = new StringBuilder("insert into daftar_buku (judul_buku, penulis_buku) values");
    for (int i = 0; i < books.size(); i++) {
      sql.append(" (?, ?)");
      if (books.size() > 1 && i < books.size() - 1) sql.append(",");
    }

    PreparedStatement st = connection.prepareStatement(sql.toString());

    for (int i = 0; i < books.size(); i++) {
      st.setString(i * 2 + 1, books.get(i).getTitle());
      st.setString(i * 2 + 2, books.get(i).getAuthor());
    }

    st.execute();
    connection.close();
  }

  public void removeAll(List<Book> books) throws SQLException {
    Connection connection = DriverManager.getConnection("jdbc:sqlite:db");
    StringBuilder sql = new StringBuilder("delete from daftar_buku where kode_buku in (");
    for (int i = 0; i < books.size(); i++) {
      sql.append("?");
      if (books.size() > 1 && i < books.size() - 1) sql.append(", ");
    }
    sql.append(")");

    PreparedStatement st = connection.prepareStatement(sql.toString());

    for (int i = 0; i < books.size(); i++) {
      st.setString(i + 1, books.get(i).getCode());
    }

    st.executeUpdate();
    connection.close();
  }

  public void update(Book book) throws SQLException {
    Connection connection = DriverManager.getConnection("jdbc:sqlite:db");
    String sql = "update daftar_buku set judul_buku = ?, penulis_buku = ? where kode_buku = ?";

    PreparedStatement st = connection.prepareStatement(sql);

    st.setString(1, book.getTitle());
    st.setString(2, book.getAuthor());
    st.setString(3, book.getCode());

    st.executeUpdate();
    connection.close();
  }

}
