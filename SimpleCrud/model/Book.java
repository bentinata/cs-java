package model;

import javafx.beans.property.SimpleStringProperty;

public class Book {
  private SimpleStringProperty title;
  private SimpleStringProperty author;
  private SimpleStringProperty code;

  public Book() {
    this.title = new SimpleStringProperty("");
    this.author = new SimpleStringProperty("");
    this.code = new SimpleStringProperty("");
  }

  public String getTitle() {
    return title.get();
  }

  public SimpleStringProperty titleProperty() {
    return title;
  }

  public void setTitle(String title) {
    this.title.set(title);
  }

  public String getAuthor() {
    return author.get();
  }

  public SimpleStringProperty authorProperty() {
    return author;
  }

  public void setAuthor(String author) {
    this.author.set(author);
  }

  public String getCode() {
    return code.get();
  }

  public SimpleStringProperty codeProperty() {
    return code;
  }

  public void setCode(String code) {
    this.code.set(code);
  }
}