import javafx.application.Application;
import javafx.stage.Stage;
import view.View;

import java.sql.SQLException;

public class Main extends Application {
  @Override
  public void start(Stage stage) throws ClassNotFoundException, SQLException {
    stage.setTitle("No title specification.");
    stage.setScene(new View());
    stage.show();
  }
}
