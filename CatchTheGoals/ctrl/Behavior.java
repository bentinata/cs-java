package ctrl;

import javafx.animation.Interpolator;
import javafx.animation.TranslateTransition;
import javafx.scene.Scene;
import javafx.scene.shape.Path;
import javafx.scene.shape.Shape;
import javafx.util.Duration;
import model.RecordConnector;
import viw.Front;
import viw.Game;
import viw.Goal;

import java.sql.SQLException;

public class Behavior {
  public static void jump(Game game) {
    Shape person = game.getPerson();
    double start = person.getLayoutY();
    double diff = person.getParent().getLayoutBounds().getHeight() - person.getLayoutBounds().getHeight() * 1.5;
    TranslateTransition up = new TranslateTransition(Duration.millis(diff * 1.5), person);
    up.setToY(-diff);
    up.setInterpolator(Interpolator.EASE_OUT);
    TranslateTransition down = new TranslateTransition(Duration.millis(diff * 1.2), person);
    down.setToY(start);
    down.setInterpolator(Interpolator.EASE_IN);
    up.setOnFinished(e -> down.play());

    up.play();
  }

  public static void spawn(Game game) {
    Goal goal = new Goal();
    game.getChildren().add(goal);
    if (goal.getValue() == Goal.ZONK) {
      goal.setLayoutY(Math.random() * Goal.MIN_HEIGHT);
    } else {
      goal.setLayoutY(Goal.MIN_HEIGHT - goal.getValue() * Goal.MIN_HEIGHT / Goal.MAX);
    }
    goal.setLayoutX(-100);
    TranslateTransition animation = new TranslateTransition(Duration.seconds(2 + Math.random() * 3), goal);
    animation.setFromY(goal.getLayoutY());
    animation.setToY(goal.getLayoutY());
    double rightmost = game.getWidth() + goal.getEllipse().getRadiusX() * 4;
    if (Math.random() < .5) {
      animation.setFromX(0);
      animation.setToX(rightmost);
    } else {
      animation.setFromX(rightmost);
      animation.setToX(-rightmost);
    }
    animation.setInterpolator(Interpolator.LINEAR);
    animation.setOnFinished(e -> game.getChildren().remove(goal));

    goal.boundsInParentProperty().addListener((observable, oldValue, newValue) -> {
      if (((Path) Shape.intersect(game.getPerson(), (Shape) goal.getEllipse())).getElements().size() > 0) {
        game.getChildren().remove(goal);
        if (goal.getValue() == Goal.ZONK) wq(game);
        if (!goal.isHit()) game.getRecord().setScore(game.getRecord().getScore() + goal.getValue());
        goal.doHit();
      }
    });

    animation.play();
  }

  public static void key(Scene scene) {
    scene.setOnKeyPressed(e -> {
      switch (e.getCode()) {
        case SPACE:
          wq((Game) scene.getRoot());
          break;
        case UP:
          jump((Game) scene.getRoot());
          break;
      }
    });
  }

  public static void wq(Game game) {
    try {
      RecordConnector recordConnector = new RecordConnector();
      recordConnector.update(game.getRecord());
      game.getScene().setRoot(new Front());
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
}
