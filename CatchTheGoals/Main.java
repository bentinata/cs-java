import ctrl.Behavior;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import viw.Front;

import java.sql.SQLException;

public class Main extends Application {
  @Override
  public void start(Stage stage) throws ClassNotFoundException, SQLException {
    stage.setTitle("No title specification.");
    stage.setScene(new Scene(new Front(), 600, 400));
//    stage.setScene(new Scene(new Game(new Record("Ben", 0)), 600, 400));
    Behavior.key(stage.getScene());
    stage.show();
  }
}
