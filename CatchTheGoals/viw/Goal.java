package viw;

import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.text.Text;
import javafx.scene.text.TextBoundsType;

public class Goal extends StackPane {
  public static final int ZONK = 0;
  public static final int MAX = 50;
  public static final int MIN_HEIGHT = 120;
  private int value;
  private Ellipse goal;
  private boolean hit = false;

  public Goal() {
    goal = new Ellipse();
    goal.setRadiusX(30);
    goal.setRadiusY(20);


    if (Math.random() > .1)
      value = (int) (Math.random() * MAX);
    else value = ZONK;

    if (value > ZONK)
      goal.setFill(Color.color(
        .5 + Math.random() * .5,
        .5 + Math.random() * .5,
        .5 + Math.random() * .5
      ).saturate().saturate().saturate());
    else goal.setFill(Color.BLACK);

    Text overlay = new Text("" + value);
    overlay.setBoundsType(TextBoundsType.VISUAL);

    getChildren().addAll(goal, overlay);
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  public Ellipse getEllipse() {
    return goal;
  }

  public boolean isHit() {
    return hit;
  }

  public boolean doHit() {
    hit = true;
    return hit;
  }
}
