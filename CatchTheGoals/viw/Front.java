package viw;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import model.Record;
import model.RecordConnector;

import java.sql.SQLException;
import java.util.List;

public class Front extends BorderPane {
  public Front() throws ClassNotFoundException, SQLException {
    super();
    Insets inset = new Insets(8, 8, 8, 8);

    TableView<Record> table = new TableView<>();
    RecordConnector recordConnector;
    recordConnector = new RecordConnector();
    table.setItems(recordConnector.getAll());

//    Top part.
    Label heading = new Label("Catch The Goals");
    heading.setPadding(inset);
    BorderPane.setAlignment(heading, Pos.CENTER);
    setTop(heading);

//    Center part.
    TextField name = new TextField();
    name.setPromptText("Your name");
    name.setPrefWidth(120);
    Button play = new Button("Play");
    play.setPrefWidth(80);
    play.setOnAction(e -> {
      Record record = new Record(name.getText(), 0);
      List<Record> filtered = table.getItems().filtered(x -> x.getName().equals(name.getText()));
      if (filtered.size() > 0) {
        record = filtered.get(0);
      } else {
        try {
          RecordConnector c = new RecordConnector();
          c.add(record);
        } catch (ClassNotFoundException e1) {
          e1.printStackTrace();
        } catch (SQLException e1) {
          e1.printStackTrace();
        }
      }

      getScene().setRoot(new Game(record));
    });
    HBox playHbox = new HBox(inset.getTop(), name, play);
    playHbox.setAlignment(Pos.CENTER_RIGHT);
    setCenter(playHbox);

//    Bottom part.
    TableColumn<Record, String> nameCol = new TableColumn<>("Name");
    nameCol.setCellValueFactory(x -> x.getValue().nameProperty());
    TableColumn<Record, Integer> scoreCol = new TableColumn<>("Score");
    scoreCol.setCellValueFactory(x -> x.getValue().scoreProperty().asObject());
    table.getColumns().setAll(nameCol, scoreCol);
    setBottom(table);

    getChildren().forEach(node -> BorderPane.setMargin(node, inset));
  }
}
