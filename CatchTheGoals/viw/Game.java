package viw;

import ctrl.Behavior;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.util.Duration;
import model.Record;

public class Game extends Pane {
  private Rectangle person = new Rectangle(300, 350, 20, 50);
  private Record record;

  public Game(Record record) {
    super();
    this.record = record;
    getChildren().add(person);

    // Animation
    Timeline timeline = new Timeline(
      new KeyFrame(Duration.seconds(1), e -> {
        Behavior.spawn(this);
      })
    );
    timeline.setCycleCount(Timeline.INDEFINITE);
    timeline.play();
  }

  public Shape getPerson() {
    return person;
  }

  public Record getRecord() {
    return record;
  }
}
