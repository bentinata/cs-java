package model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;

public class RecordConnector {
  public RecordConnector() throws ClassNotFoundException {
    Class.forName("org.sqlite.JDBC");
  }

  public ObservableList<Record> getAll() throws SQLException {
    Connection connection = DriverManager.getConnection("jdbc:sqlite:db");
    ObservableList<Record> records = FXCollections.observableArrayList();
    ResultSet resultSet = connection
      .createStatement()
      .executeQuery("select * from record");
    while (resultSet.next()) {
      Record record = new Record(
        resultSet.getString("name"),
        resultSet.getInt("score")
      );
      records.add(record);
    }

    connection.close();
    return records;
  }

  public void add(Record record) throws SQLException {
    Connection connection = DriverManager.getConnection("jdbc:sqlite:db");
    String sql = "insert into record (name, score) values (?, ?)";
    PreparedStatement st = connection.prepareStatement(sql.toString());

    st.setString(1, record.getName());
    st.setInt(2, record.getScore());

    st.execute();
    connection.close();
  }

  public void update(Record record) throws SQLException {
    Connection connection = DriverManager.getConnection("jdbc:sqlite:db");
    String sql = "update record set score = ? where name = ?";

    PreparedStatement st = connection.prepareStatement(sql);

    st.setInt(1, record.getScore());
    st.setString(2, record.getName());

    st.executeUpdate();
    connection.close();
  }
}
