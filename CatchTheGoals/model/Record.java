package model;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Record {
  private SimpleStringProperty name;
  private SimpleIntegerProperty score;

  public Record(String name, Integer score) {
    this.name = new SimpleStringProperty(name);
    this.score = new SimpleIntegerProperty(score);
  }

  public String getName() {
    return name.get();
  }

  public SimpleStringProperty nameProperty() {
    return name;
  }

  public int getScore() {
    return score.get();
  }

  public SimpleIntegerProperty scoreProperty() {
    return score;
  }

  public void setScore(int score) {
    this.score.set(score);
  }
}
