import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Shape;

public class Swimmer {
  private Shape shape;
  private double direction;
  private double speed;

  public Swimmer() {

  }

  public Swimmer(Shape shape) {
    setShape(shape);
  }

  public Shape getShape() {
    return shape;
  }

  public Swimmer setShape(Shape shape) {
    this.shape = shape;

    if (shape instanceof Circle || shape instanceof Polygon) {
      shape.setTranslateX(shape.getLayoutBounds().getWidth() / 2);
      shape.setTranslateY(shape.getLayoutBounds().getHeight() / 2);
    }

    return this;
  }
  
  public double getDirection() {
    return direction;
  }

  public Swimmer setDirection(double direction) {
    this.direction = direction;
    return this;
  }

  public double getSpeed() {
    return speed;
  }

  public Swimmer setSpeed(double speed) {
    this.speed = speed;
    return this;
  }

  public double flipDirection(double pivot) {
    direction = direction % 360;

    if (direction > pivot) {
      direction = pivot - (direction - pivot);
    } else if (direction < pivot) {
      direction = pivot + (pivot - direction);
    } else if (direction == pivot) {
      // Nothing happens.
    }

    if (direction < 0) {
      direction += (1 + Math.abs((int)(direction/360))) * 360;
    }

    this.direction = direction;

    return direction;
  }

  public void move() {
    double x = shape.getLayoutX();
    double y = shape.getLayoutY();
    double w = shape.getLayoutBounds().getWidth();
    double h = shape.getLayoutBounds().getHeight();
    double maxX = shape.getParent().getLayoutBounds().getWidth();
    double maxY = shape.getParent().getLayoutBounds().getHeight();

    x += speed * Math.cos(Math.toRadians(direction));
    y += speed * Math.sin(Math.toRadians(direction));

    if (x < 0) {
      x = Math.abs(x);
      flipDirection(90);
    }

    if (x + w > maxX) {
      x += maxX - (x + w);
      flipDirection(90);
    }

    if (y < 0) {
      y = Math.abs(y);
      flipDirection(0);
    }

    if (y + h > maxY) {
      y += maxY - (y + h);
      flipDirection(0);
    }


    shape.setLayoutX(x);
    shape.setLayoutY(y);
  }
}
