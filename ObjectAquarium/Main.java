import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.List;

public class Main extends Application {
  @Override
  public void start(Stage stage) {
    stage.setTitle("No Title Specification");
    List<Swimmer> swimmers = new ArrayList<>();
    Insets inset = new Insets(8, 8, 8, 8);
    BorderPane frontPane = new BorderPane();
    BorderPane mainPane = new BorderPane();

    Scene scene = new Scene(frontPane, 600, 400);

    // Configure frontPane.
    frontPane.setPadding(inset);
    Label title = new Label("CircleSwimmer Aquarium");
    BorderPane.setAlignment(title, Pos.CENTER);
    frontPane.setTop(title);

    Button start = new Button("Start");
    start.setOnAction(e -> scene.setRoot(mainPane));
    frontPane.setCenter(start);

    Label num = new Label("1503846");
    BorderPane.setAlignment(num, Pos.BOTTOM_CENTER);
    frontPane.setLeft(num);

    Label name = new Label("Benget Nata");
    BorderPane.setAlignment(name, Pos.BOTTOM_CENTER);
    frontPane.setRight(name);

    // Configure mainPane.
    Pane pool = new Pane();
    pool.setStyle("-fx-background-color: white");
    mainPane.setCenter(pool);

    HBox hbox = new HBox();
    hbox.setPadding(inset);
    hbox.setSpacing(inset.getTop());

    StackPane stackPane = new StackPane();
    Label count = new Label("Count: 0");
    StackPane.setAlignment(count, Pos.CENTER_LEFT);
    stackPane.getChildren().add(count);
    HBox.setHgrow(stackPane, Priority.ALWAYS);
    hbox.getChildren().add(stackPane);

    Button add = new Button("Add");
    add.setOnAction(e -> {
      Swimmer swimmer = new Swimmer();
      double size = Math.random() * 25 + 25;

      switch ((int) (Math.random() * 5)) {
        case 0:
          Circle circle = new Circle(size);
          swimmer.setShape(circle);
          break;

        case 1:
          Rectangle rectangle = new Rectangle(size * 2, size * 2);
          swimmer.setShape(rectangle);
          break;

        case 2:
          Rectangle rounded = new Rectangle(size * 2, size * 2);
          rounded.setArcWidth(size);
          rounded.setArcHeight(size);
          swimmer.setShape(rounded);
          break;

        case 3:
          Polygon hexagon = new Polygon();
          for (int i = 0; i < 6; i++) {
            hexagon.getPoints().add(size * Math.cos(i * 2 * Math.PI / 6));
            hexagon.getPoints().add(size * Math.sin(i * 2 * Math.PI / 6));
          }
          swimmer.setShape(hexagon);
          break;

        case 4:
          Polygon octahedron = new Polygon();
          for (int i = 0; i < 8; i++) {
            octahedron.getPoints().add(size * Math.cos(i * 2 * Math.PI / 8));
            octahedron.getPoints().add(size * Math.sin(i * 2 * Math.PI / 8));
          }
          swimmer.setShape(octahedron);
          break;
      }

      swimmer.getShape().setLayoutX(Math.random() * pool.getWidth());
      swimmer.getShape().setLayoutY(Math.random() * pool.getHeight());
      Color color = Color.color(Math.random(), Math.random(), Math.random());
      swimmer.getShape().setFill(color);
      swimmer.getShape().setStroke(color.darker());
      swimmer.getShape().setStrokeWidth(2);
//      Uncomment this to make it truly 360.
//      swimmer.setDirection(Math.random() * 360);
      swimmer.setDirection(45 + (int) (Math.random() * 4) * 90);
      swimmer.setSpeed(Math.random() * 5 + 5);
      swimmers.add(swimmer);
      pool.getChildren().add(swimmer.getShape());
      count.setText("Count: " + swimmers.size());
    });
    hbox.getChildren().add(add);

    Button exit = new Button("Exit");
    exit.setOnAction(e -> Platform.exit());
    hbox.getChildren().add(exit);

    mainPane.setTop(hbox);

    // Show
    stage.setScene(scene);
    stage.show();


    // Animation
    Timeline timeline = new Timeline();
    timeline.setCycleCount(Timeline.INDEFINITE);
    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(1000 / 60), e ->
      swimmers.forEach(swimmer -> swimmer.move())
    ));
    timeline.play();
  }
}
